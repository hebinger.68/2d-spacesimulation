﻿using UnityEngine;

public class SpaceTimeHandler : MonoBehaviour
{
    public double globalTime;
    public float globalSpeed;

    public float G = 6.67408e-11f;

    public float renderScale = 1;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        globalTime += Time.deltaTime * globalSpeed;
    }




}
