﻿using UnityEngine;

public static class Vector2DoubleExtention
{
    public static Vector2Double ToVector2Double(this Vector3 v)
    {
        return new Vector2Double(v.x, v.y);
    }

}


[System.Serializable]
public class Vector2Double
{

    public double x;
    public double y;

    public Vector2Double(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2Double()
    {
        this.x = 0;
        this.y = 0;
    }



    public Vector2 toVector2()
    {
        return new Vector2((float)this.x, (float)this.y);
    }

    public Vector3 toVector3(float z)
    {
        return new Vector3((float)this.x, (float)this.y, z);
    }

    public Vector3 toVector3()
    {
        return new Vector3((float)this.x, (float)this.y, 0);
    }

    public static Vector2Double operator +(Vector2Double vect1, Vector2Double vect2)
    {
        return new Vector2Double(vect1.x + vect2.x, vect1.y + vect2.y);
    }

    public static Vector2Double operator -(Vector2Double vect1, Vector2Double vect2)
    {
        return new Vector2Double(vect1.x - vect2.x, vect1.y - vect2.y);
    }

    public static Vector2Double operator *(Vector2Double vect1, double multiplyer)
    {
        return new Vector2Double(vect1.x * multiplyer, vect1.y * multiplyer);
    }

    public static Vector2Double operator /(Vector2Double vect1, double multiplyer)
    {
        return new Vector2Double(vect1.x / multiplyer, vect1.y / multiplyer);
    }



    public double getMagnitude()
    {
        return System.Math.Sqrt(this.x * this.x + this.y * this.y);
    }

    public double getSqrMagnetude()
    {
        return (this.x * this.x + this.y * this.y);
    }

    public void set(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    //maybe not dot product ???
    public static double Dot(Vector2Double v1, Vector2Double v2)
    {
        return (v1.x * v2.y) - (v1.y * v2.x);
    }


}
