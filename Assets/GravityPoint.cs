﻿using UnityEngine;

public class GravityPoint : MonoBehaviour
{
    public Vector2Double body_position;

    public float body_mass;
    public float body_radius;

    public float sphere_influance_radius;

    public bool isCenter;
    public float distance_from_parent;
    public GravityPoint parent;

    public double localTime;
    public double revolution_time;
    public double revolution_start;
    public float revolution_years;

    public SpaceTimeHandler spaceTime;
    public SpriteRenderer model;


    //meters per second relative to parent
    public double revolution_velocity;
    public Vector2Double revolution_velocity_vector;


    //rayon div par 10
    //masse div par 10²

    //1 carre = 10m

    // Use this for initialization
    void Start() {

        if (isCenter) {
            sphere_influance_radius = 0;
            revolution_velocity = 0;
        } else {
            sphere_influance_radius = distance_from_parent * Mathf.Pow(this.body_mass / parent.body_mass, 2f / 5f);
            revolution_velocity = System.Math.Sqrt((spaceTime.G * parent.body_mass) / (distance_from_parent * 1000));
            revolution_velocity_vector = new Vector2Double(-System.Math.Sin(localTime * Mathf.PI * 2 / revolution_time) * revolution_velocity, System.Math.Cos(localTime * Mathf.PI * 2 / revolution_time) * revolution_velocity);
        }


    }

    // Update is called once per frame
    void Update() {

        if (isCenter) {
            //no camera moves yet
        } else {
            localTime = spaceTime.globalTime - revolution_start;
            if (localTime >= revolution_time * 3 || localTime <= -revolution_time * 3) {
                revolution_years = Mathf.Floor((float)(spaceTime.globalTime / revolution_time));
                revolution_start = revolution_time * revolution_years;
                localTime = spaceTime.globalTime - revolution_start;
            } else if (localTime >= revolution_time) {
                localTime -= revolution_time;
                revolution_start += revolution_time;
            } else if (localTime < 0) {
                localTime += revolution_time;
                revolution_start -= revolution_time;

            }

            body_position.x = System.Math.Cos(localTime * Mathf.PI * 2 / revolution_time) * distance_from_parent;
            body_position.y = System.Math.Sin(localTime * Mathf.PI * 2 / revolution_time) * distance_from_parent;
        }

        revolution_velocity_vector = new Vector2Double(-System.Math.Sin(localTime * Mathf.PI * 2 / revolution_time) * revolution_velocity, System.Math.Cos(localTime * Mathf.PI * 2 / revolution_time) * revolution_velocity);



        this.transform.localPosition = new Vector3((float)(body_position.x / spaceTime.renderScale), (float)(body_position.y / spaceTime.renderScale), 0);


        float renderScale = body_radius / spaceTime.renderScale;
        model.transform.localScale = new Vector3(renderScale, renderScale, 1);

        Debug.DrawRay(this.transform.position, (Vector3.up * sphere_influance_radius) / spaceTime.renderScale, Color.red);
        Debug.DrawRay(this.transform.position, (Vector3.up * body_radius) / spaceTime.renderScale, Color.yellow);


    }
}
