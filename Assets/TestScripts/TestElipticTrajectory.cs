﻿using MathNet.Numerics.RootFinding;
using MathNet.Numerics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UIElements;

public class TestElipticTrajectory : MonoBehaviour
{

    const double DegToRad = Math.PI / 180;

    public Vector2Double point;
    public double point_mass;

    public CartesianParametersScriptable initialParameters;

    public Vector2Double craft_position;
    public Vector2Double craft_velocity;

    public SpaceTimeHandler spaceTime;
    [Space]
    [Space]

    public double epsilon;

    public double mu; // Gravitational parameter of the central body (e.g., Earth)

    [Range(0.0001f, 1f)]
    public double SolveEccentricAnomalyDelta = 0.001;

    public int trajectoryResolution = 200;

    public Vector3 e_vector; //exentricity vector

    public Vector3 h_vector;

    public double a, b, c; //semi-axes

    public double r, v; //cartesian magnetudes

    public double e_anomaly; //eccentric anomaly

    public double MeanAnomaly;

    public double EccentricAnomaly;

    public double TrueAnomaly;

    public double RadialDistance;

    public double T;
    public double t0;

    public double n;

    public double angle;

    [Space]

    public double h;

    public double Eccentricity;

    public ConicType Conic = ConicType.Undefined;


    public double sinh_H = 0;
    public double HyperbolicEccentricAnomaly = 0;


    void Start()
    {

        craft_position = initialParameters.parameters.position;
        craft_velocity = initialParameters.parameters.velocity;

        transform.position = craft_position.toVector3();
        point = new Vector2Double();
        point_mass = 0.0095552e+24; //   /5
        mu = (spaceTime.G * point_mass);

        //mu = 3.98574405096e+14;

        SolveConicType();

        switch (Conic)
        {
            case ConicType.Ellipse:
                SolveElipse();
                t0 = FastSolveT0Elipse();
                //t0 = SolveT0Elipse();
                break;
            case ConicType.Hyperbola:
                SolveHyperbola();
                t0 = SolveT0Hyperbolic();
                break;
            default:
                Debug.LogError("unsuported conic !"); //TODO
                break;
        }

        Debug.DrawRay(center, -e_vector.normalized * (float)a, Color.yellow, 100);
        Debug.DrawRay(center, -new Vector2Double(e_vector.y, -e_vector.x).toVector3().normalized * (float)b, Color.red, 100);

        Debug.DrawLine(center, SolveTrajectory(FromEccentricityToT(0), Conic).toVector3(), Color.white, 100);
        Debug.DrawLine(center, -e_vector.normalized * (float)a, Color.black, 100);

        int res = trajectoryResolution;
        for (int i = 0; i < res; i++)
        {
            Vector2Double p = SolveTrajectory(FromEccentricityToT(Math.PI * 2 / res * i - Math.PI), Conic);
            Vector2Double p2 = SolveTrajectory(FromEccentricityToT(Math.PI * 2 / res * (i + 1) - Math.PI), Conic);

            Debug.DrawLine(p.toVector3(), p2.toVector3(), Color.cyan, 100);

            positions.Add(p);
        }

        Update();
    }

    void Update()
    {
        this.transform.position = SolveTrajectory(spaceTime.globalTime + t0, Conic).toVector3();
    }

    private double SolveT0Hyperbolic()
    {
        //true anomaly
        double angle = Math.Acos(Vector2Double.Dot(craft_position, e_vector.ToVector2Double()) / (r * Eccentricity));

        //anomalie eccentric hyperbolic
        double H = 2.0 * Math.Atanh(Math.Sqrt((Eccentricity + 1.0) / (Eccentricity - 1.0)) * Math.Tan(angle / 2.0));

        double M = Eccentricity * Math.Sinh(H) - H;

        return -M / n;

    }

    public double testfast = 0;

    private double FastSolveT0Elipse()
    {

        //https://en.wikipedia.org/wiki/True_anomaly

        double TrueAnomaly = Math.Acos(Vector2.Dot(e_vector, craft_position.toVector2()) / (e_vector.magnitude * craft_position.getMagnitude()));

        if (Vector2.Dot(craft_position.toVector2(), craft_velocity.toVector2()) < 0)
        {
            TrueAnomaly = 2 * Math.PI - TrueAnomaly;
        }

        testfast = Vector2.Dot(craft_position.toVector2(), craft_velocity.toVector2());

        E0 = 2 * Math.Atan(
            (Math.Sqrt((-Eccentricity - 1) / (Eccentricity - 1)) * Math.Tan(TrueAnomaly / 2) - Math.Sqrt((-Eccentricity - 1) / (Eccentricity - 1)) * Eccentricity * Math.Tan(TrueAnomaly / 2)
            ) / (Eccentricity + 1)
            );

        double M = E0 - Eccentricity * Math.Sin(E0);

        return M / n + T;
    }


    private double SolveT0Elipse()
    {

        int i = 0;

        double E1 = (2 * Math.PI);
        double E2 = (2 * Math.PI) / 2;

        Vector2Double test1 = SolveTrajectory(FromEccentricityToT(E1), Conic);
        Vector2Double test2 = SolveTrajectory(FromEccentricityToT(E2), Conic);

        if ((test1 - craft_position).getMagnitude() < (test2 - craft_position).getMagnitude())
        {
            E1 = (2 * Math.PI) * 5 / 4;
            E2 = (2 * Math.PI) * 3 / 4;
        }
        else
        {
            E1 = (2 * Math.PI) * 1 / 4;
            E2 = (2 * Math.PI) * 3 / 4;
        }

        do
        {

            test1 = SolveTrajectory(FromEccentricityToT(E1), Conic);
            test2 = SolveTrajectory(FromEccentricityToT(E2), Conic);

            if ((test1 - craft_position).getMagnitude() < (test2 - craft_position).getMagnitude())
            {
                E2 = E2 - ((E2 - E1) / 2);
            }
            else
            {
                E1 = E1 + ((E2 - E1) / 2);
            }

            Debug.DrawLine(test1.toVector3(), Vector3.zero, Color.blue, 100);


        } while (i++ < 30);

        return FromEccentricityToT(E1);

    }

    private double FromEccentricityToT(double Et)
    {
        double M0 = Et - Eccentricity * Math.Sin(Et);
        return M0 / n;
    }

    public List<Vector2Double> positions;

    private Vector2Double SolveTrajectory(double t, ConicType conic)
    {
        if (conic == ConicType.Ellipse)
        {
            return SolveTrajectoryEllipse(t);
        }
        if (conic == ConicType.Hyperbola)
        {
            return SolveTrajectoryHyperbola(t);
        }
        return null;
    }

    public double testm = 0;

    private Vector2Double SolveTrajectoryHyperbola(double t)
    {
        MeanAnomaly = n * (t);

        testm = n * -0.2805;

        HyperbolicEccentricAnomaly = -Math.Sign(h_vector.z) * KeplerSolveHyperbolicEccentricAnomaly(MeanAnomaly, Eccentricity, SolveEccentricAnomalyDelta, tries);

        //Calculate the hyperbolic true anomaly (ν)
        //sinh_H = Math.Sinh(HyperbolicEccentricAnomaly);
        //TrueAnomaly = Math.Asinh(sinh_H / Math.Sqrt(Math.Pow(Eccentricity, 2) - 1));

        TrueAnomaly = 2 * Math.Atan(Math.Sqrt((Eccentricity + 1) / (Eccentricity - 1)) * Math.Tanh(HyperbolicEccentricAnomaly / 2));

        //Calculate the distance (r) from the central body
        r = a * (1 - Math.Pow(Eccentricity, 2)) / (1 + Eccentricity * Math.Cos(TrueAnomaly));

        //Convert to Cartesian coordinates
        Vector2Double position_norm = new Vector2Double();

        position_norm.x = r * Math.Cos(TrueAnomaly);
        position_norm.y = r * Math.Sin(TrueAnomaly);

        // Angle of rotation in radians
        double angleInRadians = Math.Atan2(e_vector.y, e_vector.x);

        // Calculate the cosine and sine of the angle
        double cosTheta = Math.Cos(angleInRadians);
        double sinTheta = Math.Sin(angleInRadians);

        Vector2Double position = new Vector2Double();

        // Perform the rotation
        position.x = position_norm.x * cosTheta - position_norm.y * sinTheta;
        position.y = position_norm.x * sinTheta + position_norm.y * cosTheta;

        return position;
    }

    private Vector2Double SolveTrajectoryEllipse(double t)
    {
        //Calculate Mean Anomaly(M):
        MeanAnomaly = n * (t);

        EccentricAnomaly = -Math.Sign(h_vector.z) * SolveEccentricAnomaly(MeanAnomaly, Eccentricity);

        TrueAnomaly = 2 * Math.Atan(Math.Sqrt((1 + Eccentricity) / (1 - Eccentricity)) * Math.Tan(MeanAnomaly / 2)); // (ν)

        double x = a * (Math.Cos(EccentricAnomaly) - e_vector.magnitude);
        double y = -b * (Math.Sin(EccentricAnomaly));

        double rads = angle * DegToRad;

        double x_ = x * Math.Cos(rads) - y * Math.Sin(rads);
        double y_ = x * Math.Sin(rads) + y * Math.Cos(rads);

        return new Vector2Double(x_, y_);
    }

    /// <summary>
    /// Solve Kepler's Equation for Eccentric Anomaly (E)
    /// </summary>
    public double SolveEccentricAnomaly(double Mean_anomaly, double Eccentricity)
    {
        double E = 0;
        while (Mean_anomaly > E - Eccentricity * Math.Sin(E))
        {
            E = E + SolveEccentricAnomalyDelta;
        }
        return E;
    }


    public int tries = 10;


    static double KeplerSolveHyperbolicEccentricAnomaly(double M, double e, double tol = 1e-8, int maxIter = 100)
    {
        double E = M; // Valeur initiale

        double E_new = 0;

        for (int i = 0; i < maxIter; i++)
        {
            E_new = E - (E - e * Math.Sinh(E) - M) / (1 - e * Math.Cosh(E));
            if (Math.Abs(E_new - E) < tol)
            {
                return E_new;
            }
            E = E_new;
        }

        return E_new;
    }

    private void SolveElipse()
    {
        //Semi-Major Axis (a)
        a = -mu / (2 * epsilon);

        //Calculate Orbital Period(T)
        T = (2 * Math.PI) * Math.Sqrt((a * a * a) / mu);

        //Calculate Mean Motion (n)
        n = (2 * Math.PI) / T;

        b = a * Math.Sqrt(1 - (Eccentricity * Eccentricity));
        //M0 = Math.PI + Math.Acos(Vector2Double.Dot(craft_position, e_vector.ToVector2Double()) / (e_vector.magnitude * craft_position.getMagnetude()));
        angle = Vector2.SignedAngle(Vector2.right, e_vector.ToVector2Double().toVector2());

        center = point.toVector3() - e_vector * (float)a;

    }

    private void SolveHyperbola()
    {
        //Semi-Major Axis (a)
        a = -mu / (2 * epsilon);

        n = Math.Sqrt(mu / Math.Pow(-a, 3));

    }

    private void SolveConicType()
    {
        //Calculate specific energy (ε) epsilon
        r = craft_position.getMagnitude();
        v = craft_velocity.getMagnitude();

        epsilon = (Math.Pow(v, 2) / 2) - (mu / r);

        //Calculate specific angular momentum (h)
        h_vector = Vector3.Cross(craft_position.toVector3(), craft_velocity.toVector3());
        h = h_vector.magnitude;

        //Calculate eccentricity (e) alternative
        //Eccentricity = Math.Sqrt(1 + (2 * epsilon * Math.Pow(h, 2)) / (Math.Pow(mu, 2)));

        e_vector = (1 / (float)mu) * (Vector3.Cross(craft_velocity.toVector3(), h_vector) - (float)mu * (craft_position.toVector3() / (float)r));
        Eccentricity = e_vector.magnitude;

        //Determine the conic type

        if (Eccentricity == 0)
        {
            Conic = ConicType.Circular; // not implemented (virtualy imposible case)
        }
        else if (epsilon < 0 && Eccentricity < 1)
        {
            Conic = ConicType.Ellipse;
        }
        else if (epsilon == 0 && Eccentricity == 1)
        {
            Conic = ConicType.Parabola; // not implemented (virtualy imposible case)
        }
        else if (epsilon > 0 && Eccentricity > 1)
        {
            Conic = ConicType.Hyperbola;
        }
        else
        {
            Conic = ConicType.Undefined;
            Debug.LogError("conic_type Undefined");
        }
    }

    public Vector3 center;
    public double E0;
    public double M0;

    public enum ConicType
    {
        Undefined = 0,
        Ellipse,
        Circular,
        Parabola,
        Hyperbola,
    }
}
