﻿using UnityEngine;

public class TestGravityTrajectory : MonoBehaviour
{

    public Vector2Double point;
    public double point_mass;


    public CartesianParametersScriptable initialParameters;

    public Vector2Double craft_position;
    public Vector2Double craft_velocity;


    public SpaceTimeHandler spaceTime;


    public double mu;

    // Start is called before the first frame update
    void Start()
    {
        craft_position = initialParameters.parameters.position;
        craft_velocity = initialParameters.parameters.velocity;


        point = new Vector2Double();
        point_mass = 0.0095552e+24; //   /5

        mu = (spaceTime.G * point_mass);

        //mu = 3.98574405096e+14;

    }

    // Update is called once per frame
    void Update()
    {


        Vector2Double acceleration = GravityAcceleration();

        Debug.DrawRay(transform.position, acceleration.toVector3() * 0.1f, Color.yellow);


        craft_velocity += acceleration * Time.deltaTime * spaceTime.globalSpeed;

        craft_position += craft_velocity * Time.deltaTime * spaceTime.globalSpeed;


        this.transform.position = craft_position.toVector3();

        Debug.DrawRay(transform.position, Vector3.down * 20, Color.red, 50);




    }

    private Vector2Double GravityAcceleration()
    {
        Vector2 unit = this.transform.position - point.toVector3();
        unit.Normalize();

        Debug.DrawRay(point.toVector3(), new Vector3(unit.x, unit.y, 0));

        //float r2 = Vector2.SqrMagnitude(this.transform.position - point.transform.position);
        double r2 = System.Math.Pow(craft_position.x - point.x, 2) + System.Math.Pow(craft_position.y - point.y, 2);

        double rcx = (craft_position.x - point.x) / System.Math.Sqrt(r2);
        double rcy = (craft_position.y - point.y) / System.Math.Sqrt(r2);

        double acceleration_x = -(mu * unit.x) / r2;
        double acceleration_y = -(mu * unit.y) / r2;



        //Debug.Log(Time.deltaTime);

        return new Vector2Double(acceleration_x, acceleration_y);

    }
}
