﻿using UnityEngine;

public class TestConverterTrajectory : MonoBehaviour
{

    public Vector2Double test;
    private Vector2Double result;
    public double angle;

    // Start is called before the first frame update
    void Start() {
        result = new Vector2Double();
    }

    // Update is called once per frame
    void Update() {
        double rads = angle * System.Math.PI / 180;

        result.x = System.Math.Cos(rads) * test.x - System.Math.Sin(rads) * test.y;
        result.y = System.Math.Sin(rads) * test.x + System.Math.Cos(rads) * test.y;

        Debug.DrawRay(this.transform.position, result.toVector3(), Color.red);
        Debug.DrawRay(this.transform.position, test.toVector3(), Color.blue);
    }
}
