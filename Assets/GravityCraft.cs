﻿using UnityEngine;

public class GravityCraft : MonoBehaviour
{
    public GravityPoint point;
    public SpaceTimeHandler spaceTime;


    //100velocity max   2000position max

    //global position
    public Vector2Double craft_position;

    //relative to parent
    //public Vector2Double craft_velocity; //TODO !!! faire avec ca car plus précis meme si pas tres utile mais remetre en rb2d des qu'il y a une collision comme ca on récupere la nouvelle velocity apres



    // Use this for initialization
    void Start() {
        this.gameObject.AddComponent<Rigidbody2D>();
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
        this.GetComponent<Rigidbody2D>().mass = 1;
    }

    // Update is called once per frame
    void Update() {
        MoveWithParent();

        Vector2Double acceleration = GravityAcceleration();

        Vector2 velocity = this.GetComponent<Rigidbody2D>().velocity;
        velocity += acceleration.toVector2();

        Debug.DrawRay(this.transform.position, acceleration.toVector3(), Color.red, 10);
        Debug.DrawRay(this.transform.position, new Vector3(velocity.x, velocity.y, 0), Color.yellow);

        this.GetComponent<Rigidbody2D>().velocity = velocity;
    }


    private double previous_point_position_x;
    private double previous_point_position_y;

    private void MoveWithParent() {
        craft_position.x += point.body_position.x - previous_point_position_x;
        craft_position.y += point.body_position.y - previous_point_position_y;
        previous_point_position_x = point.body_position.x;
        previous_point_position_y = point.body_position.y;
    }


    private Vector2Double GravityAcceleration() {
        Vector2 unit = this.transform.position - point.transform.position;
        unit.Normalize();

        Debug.DrawRay(point.transform.position, new Vector3(unit.x, unit.y, 0));

        //float r2 = Vector2.SqrMagnitude(this.transform.position - point.transform.position);
        double r2 = System.Math.Pow(craft_position.x - point.body_position.x, 2) + System.Math.Pow(craft_position.y - point.body_position.y, 2);

        double rcx = (craft_position.x - point.body_position.x) / System.Math.Sqrt(r2);
        double rcy = (craft_position.y - point.body_position.y) / System.Math.Sqrt(r2);

        double acceleration_x = -(spaceTime.G * point.body_mass * unit.x * Time.deltaTime) / r2;
        double acceleration_y = -(spaceTime.G * point.body_mass * unit.y * Time.deltaTime) / r2;

        return new Vector2Double(acceleration_x, acceleration_y);
    }
}
