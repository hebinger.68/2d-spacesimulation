using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[System.Serializable]
public class Vector3Double
{

    public double x;
    public double y;
    public double z;

    public Vector3Double(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3Double()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }


    public static Vector3Double Cross(Vector3Double a, Vector3Double b)
    {
        Vector3Double c = new Vector3Double();
        c.x = a.y * b.z - a.z * b.y;
        c.y = a.z * b.x - a.x * b.z;
        c.z = a.x * b.y - a.y * b.x;

        return c;
    }

    public double getMagnetude()
    {
        return System.Math.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public static Vector3Double operator *(Vector3Double v, double multiplyer)
    {
        return new Vector3Double(v.x * multiplyer, v.y * multiplyer, v.z * multiplyer);
    }
    public static Vector3Double operator *(double multiplyer, Vector3Double v)
    {
        return new Vector3Double(v.x * multiplyer, v.y * multiplyer, v.z * multiplyer);
    }

    public static Vector3Double operator /(Vector3Double v, double multiplyer)
    {
        return new Vector3Double(v.x / multiplyer, v.y / multiplyer, v.z / multiplyer);
    }

    public static Vector3Double operator -(Vector3Double a, Vector3Double b)
    {
        return new Vector3Double(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public Vector2Double ToVector2Double(Vector3Double a)
    {
        return new Vector2Double(a.x, a.y);
    }


}

public static class Vector3DoubleExtentions
{
    public static Vector3Double ToVector3Double(this Vector3 v)
    {
        return new Vector3Double(v.x, v.y, v.z);
    }

    public static Vector3Double ToVector3Double(this Vector2Double v)
    {
        return new Vector3Double(v.x, v.y, 0);
    }

}