using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class CartesianParameters
{
    public Vector2Double position;

    public Vector2Double velocity;

}


[CreateAssetMenu(fileName = "CartesianParameters", menuName = "Custom/CartesianParameters")]
public class CartesianParametersScriptable : ScriptableObject
{
    public CartesianParameters parameters;

}