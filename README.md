# 2D Space Simulation

A 2D implementation of Keplers equations in C#

features:

- Hyperbolic orbits
- Eliptic Orbits
- Solve initial Keplerian elements from cartesian elements
- Trace space normalized trajectory for any orbit
- A Newtonian implementation to compare results (based on integration)
- Visualisation in unity

Keplers equations are virtuali free of error over time, unlike Newtons equations

Todo:

- extend Hyberbolic orbit trajectory prediction
- spheres of influence
- moving origin
- hohmann transfer solving
- speed optimisations (it would obfuscate the already complicated equations during developpment)
